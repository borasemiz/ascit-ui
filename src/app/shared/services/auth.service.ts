import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { UserCredentials } from '../models/user-credentials';
import { StoreService } from './store.service';

export interface IAuthMessage {
  code: number;
  message: string;
  error?: string;
}

@Injectable()
export class AuthService {
  private _credentials: UserCredentials;
  public redirectUrl: string = '/';

  public get credentials(): UserCredentials {
    return this._credentials;
  }

  constructor(
    private store: StoreService,
    private http: HttpClient
  ) {
    // TODO: Refactor needed in future.
    this._credentials = store.getSavedUserCredentials();
  }

  public authenticate(username: string, password: string, save: boolean = false): Promise<IAuthMessage> {
    return this.http.post<any>(`${environment.serverUrl}/auth?format=json`, {
      username, password
    }).toPromise().then(resp => {
      if (resp.token) {
        this._credentials = {
          token: resp.token,
          userId: resp.userId,
          expiration: new Date(resp.expiration).valueOf()
        };
        if (save) {
          this.store.setSavedUserCredentials(this._credentials);
        }
        return {
          code: 200,
          message: 'OK'
        }
      }
      return resp;
    }).catch(error => error.error);
  }

  public isAuthNeeded(): boolean {
    if (!this._credentials) {
      return true;
    }
    if (Date.now() >= this._credentials.expiration.valueOf()) {
      this._credentials = null;
      return true;
    }
    return false;
  }
}
