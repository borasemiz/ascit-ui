import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthService } from './auth.service';

@Injectable()
export class ApiInterceptorService implements HttpInterceptor {

  constructor(
    private authService: AuthService
  ) { }

  public intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let headers = req.headers;
    if (this.authService.credentials) {
      headers = headers.append('Authorization', `Bearer ${this.authService.credentials.token}`);
    }
    const alteredRequest = req.clone({
      headers
    });
    return next.handle(alteredRequest);
  }

}
