import { Injectable } from '@angular/core';

import { UserCredentials } from '../models/user-credentials';

const USER_CREDENTIALS_KEY = 'userCredentials';

interface IStoredObject<T> {
  object: T;
  expire: number;
}

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor() { }

  public getSavedUserCredentials(): UserCredentials {
    const storedObject = JSON.parse(window.localStorage.getItem(USER_CREDENTIALS_KEY)) as IStoredObject<UserCredentials>;
    if (!storedObject || storedObject.expire <= new Date().valueOf()) {
      return null;
    }
    return storedObject.object;
  }

  public setSavedUserCredentials(credentials: UserCredentials): void {
    const storedObject: IStoredObject<UserCredentials> = {
      object: credentials,
      expire: new Date().valueOf() + 1000 * 60
    };
    window.localStorage.setItem(
      USER_CREDENTIALS_KEY,
      JSON.stringify(storedObject)
    );
  }

}
