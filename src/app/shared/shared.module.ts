import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { InputSelectComponent, PieChartComponent } from './components';
import { AuthService, StoreService, AuthGuardService, ApiInterceptorService } from './services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
  ],
  declarations: [
    InputSelectComponent,
    PieChartComponent
  ],
  exports: [
    InputSelectComponent,
    PieChartComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptorService,
      multi: true
    },
    AuthService,
    StoreService,
    AuthGuardService
  ]
})
export class SharedModule {}
