export interface UserCredentials {
  token: string;
  userId: number;
  expiration: number;
}
