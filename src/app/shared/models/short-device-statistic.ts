export interface ShortDeviceStatistic {
  machineId: number;
  lastReceivedInfoDate: number;
  productName: string;
  targetProductionAmount: number;
  currentProductionAmount: number;
  rpm: number;
  isWorking: boolean;
}
