export interface ProductInfo {
  productId: number;
  type: string;
  description: string;
  weight: number;
}
