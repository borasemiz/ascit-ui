import { DeviceMessageLog } from './device-message-log';

export class DevicePauseAnalysis {
  currentShiftTime: number;
  dailyTime: number;
  weeklyTime: number;
  monthlyTime: number;
  allTime: number;

  currentShiftAmount: number;
  dailyAmount: number;
  weeklyAmount: number;
  monthlyAmount: number;
  allAmount: number;

  previousLog: DeviceMessageLog;

  constructor() {
    this.currentShiftAmount = 0;
    this.currentShiftTime = 0;
    this.dailyAmount = 0;
    this.dailyTime = 0;
    this.weeklyAmount = 0;
    this.weeklyTime = 0;
    this.monthlyAmount = 0;
    this.monthlyTime = 0;
    this.allAmount = 0;
    this.allTime = 0;
    this.previousLog = null;
  }
}
