import { DeviceMessageLog } from './device-message-log';

export class DeviceWorkTimeAnalysis {
  currentShiftTime: number;
  dailyTime: number;
  weeklyTime: number;
  monthlyTime: number;
  allTime: number;

  previousLog: DeviceMessageLog;

  constructor() {
    this.currentShiftTime = 0;
    this.dailyTime = 0;
    this.weeklyTime = 0;
    this.monthlyTime = 0;
    this.allTime = 0;
    this.previousLog = null;
  }
}