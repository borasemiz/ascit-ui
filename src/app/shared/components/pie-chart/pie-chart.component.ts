import { Component, Input } from '@angular/core';

@Component({
  selector: 'as-pie-chart',
  templateUrl: 'pie-chart.component.html',
  styleUrls: [ 'pie-chart.component.scss' ]
})
export class PieChartComponent {
  private _percentage: number = 0;

  public circleRotation: number;
  public circleFinishX: number;
  public circleFinishY: number;

  public circleCenterX: number = 50;
  public circleCenterY: number = 50;
  public circleRadius: number = 48;

  @Input()
  public set percentage(p: number) {
    if (p && this._percentage !== p) {
      this._percentage = p;
      this.initializeChartParameters();
    }
  }

  public get percentage(): number {
    return this._percentage;
  }

  public get pieDataString(): string {
    if (this._percentage >= 100) {
      return '';
    }
    const largeArcFlag = this.percentage ? (this.percentage > 50 ? 1 : 0) : 0;
    const line = `M ${this.circleCenterX} ${this.circleCenterY} L ${this.circleCenterX} ${this.circleCenterY - this.circleRadius}`;
    const arc = `A ${this.circleRadius} ${this.circleRadius} ${this.circleRotation} ${largeArcFlag} 1 ${this.circleFinishX} ${this.circleFinishY}`;
    return `${line} ${arc} z`;
  };

  public get outerCircleFill(): string {
    return this._percentage >= 100 ? 'white' : 'transparent';
  }

  private initializeChartParameters(): void {
    const radians = (2 * Math.PI) * (this._percentage / 100)
    this.circleRotation = radians * 180;
    this.circleFinishX = this.circleCenterX + Math.sin(radians) * this.circleRadius;
    this.circleFinishY = this.circleCenterY - Math.cos(radians) * this.circleRadius;
  }
}
