import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../shared/services';

@Component({
  selector: 'app-auth-login',
  templateUrl: 'login.component.html',
  styleUrls: [ 'login.component.scss' ]
})
export class LoginComponent {

  public loginForm: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  public onSubmitForm(formEvent: Event): void {
    formEvent.preventDefault();
    const formValues = this.loginForm.value;
    this.authService.authenticate(formValues.username, formValues.password, true).then(result => {
      if (result.code === 200) {
        this.router.navigateByUrl(this.authService.redirectUrl);
      } else {
        window.alert(result.error);
      }
    });
  }

}
