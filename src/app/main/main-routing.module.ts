import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MachineListComponent } from './machine-list/machine-list.component';
import { MainComponent } from './main.component';
import { MachineDetailComponent } from './machine-detail/machine-detail.component';
import { ProductService } from './services';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [ ProductService ],
    children: [
      {
        path: 'machine-list',
        component: MachineListComponent
      },
      {
        path: 'machine-detail/:machineId',
        component: MachineDetailComponent
      },
      {
        path: '',
        redirectTo: 'machine-list',
        pathMatch: 'prefix'
      }
    ]
  },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class MainRoutingModule {}
