import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { DeviceStatisticsService } from '../services';
import { ShortDeviceStatistic } from 'src/app/shared/models';

@Component({
  selector: 'app-machine-list',
  templateUrl: 'machine-list.component.html',
  styleUrls: [ 'machine-list.component.scss' ]
})
export class MachineListComponent implements OnInit, OnDestroy {
  private _statsSubscription: Subscription;
  private _intervalId: number = 0;

  public currentStats: ShortDeviceStatistic[] = [];

  constructor(
    private deviceStatisticsService: DeviceStatisticsService
  ) {}

  public ngOnInit(): void {
    this.setupStatusInterval();
    this._statsSubscription = this.deviceStatisticsService.generalStatsStream$.subscribe(stat => {
      this.currentStats = Object.values(stat);
    });
  }

  public ngOnDestroy(): void {
    if (this._statsSubscription) {
      this._statsSubscription.unsubscribe();
    }
    if (this._intervalId !== 0) {
      window.clearInterval(this._intervalId);
    }
  }

  public beforeTenSeconds(milliseconds: number): boolean {
    return Date.now() - milliseconds > 10000;
  }

  private setupStatusInterval(): void {
    this._intervalId = window.setInterval(() => {
      this.currentStats = this.currentStats.map(stat => {
        stat.lastReceivedInfoDate += 0;
        return stat;
      });
    }, 1000);
  }
}
