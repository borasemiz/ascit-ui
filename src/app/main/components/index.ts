export { NavbarComponent } from './navbar/navbar.component';
export { MachineCardComponent } from './machine-card/machine-card.component';
export { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
export { MachineWaitingTableComponent } from './machine-waiting-table/machine-waiting-table.component';
export { FactoryAverageAnalysisTableComponent } from './factory-average-analysis-table/factory-average-analysis-table.component';
export { ProductionHistoryComponent } from './production-history/production-history.component';
