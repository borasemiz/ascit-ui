import { Component, Input } from '@angular/core';

import { DevicePauseAnalysis } from '../../../shared/models';

@Component({
  selector: 'as-machine-waiting-table',
  templateUrl: 'machine-waiting-table.component.html',
  styleUrls: [ 'machine-waiting-table.component.scss' ]
})
export class MachineWaitingTableComponent {

  public currentlyShown: {
    amount: number;
    time: number;
  } = null;

  @Input()
  public pauseAnalysis: DevicePauseAnalysis;

  public switchTimeSpan(type: 'currentShift' | 'daily' | 'weekly' | 'monthly' | 'all'): void {
    if (this.pauseAnalysis) {
      switch(type) {
        case 'currentShift':
          this.currentlyShown = {
            amount: this.pauseAnalysis.currentShiftAmount,
            time: this.pauseAnalysis.currentShiftTime
          };
          break;
        case 'daily':
          this.currentlyShown = {
            amount: this.pauseAnalysis.dailyAmount,
            time: this.pauseAnalysis.dailyTime
          };
          break;
        case 'weekly':
          this.currentlyShown = {
            amount: this.pauseAnalysis.weeklyAmount,
            time: this.pauseAnalysis.weeklyTime
          };
          break;
        case 'monthly':
          this.currentlyShown = {
            amount: this.pauseAnalysis.monthlyAmount,
            time: this.pauseAnalysis.monthlyTime
          };
          break;
        case 'all':
          this.currentlyShown = {
            amount: this.pauseAnalysis.allAmount,
            time: this.pauseAnalysis.allTime
          };
          break;
      }
    }
  }

}
