import { Component, Input } from '@angular/core';
import { ShortDeviceStatistic } from 'src/app/shared/models';

@Component({
  selector: 'as-machine-card',
  templateUrl: 'machine-card.component.html',
  styleUrls: [ 'machine-card.component.scss' ]
})
export class MachineCardComponent {
  @Input()
  public status: ShortDeviceStatistic;

  public get efficiency(): number {
    return 100 * this.status.currentProductionAmount / this.status.targetProductionAmount;
  }  
}
