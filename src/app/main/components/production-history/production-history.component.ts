import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import * as chartjs from 'chart.js';

@Component({
  selector: 'as-production-history',
  templateUrl: 'production-history.component.html',
  styleUrls: [ 'production-history.component.scss' ]
})
export class ProductionHistoryComponent implements AfterViewInit {
  
  @ViewChild('chartContainer', { static: true })
  private _chartContainerRef: ElementRef<HTMLDivElement>

  @ViewChild('chart', { static: true })
  private _chartElementRef: ElementRef<HTMLCanvasElement>

  constructor() {}

  public mockTableData: any[] = new Array(5).fill(null).map(() => ({
    workDescription: '30/1 RING LYC KASKORSE-OZBEK',
    productionAmount: 300,
    junk: 30,
    workingTime: 1843,
    waitingTime: 13,
    startDate: new Date(2018, 6, 15),
    endDate: new Date(2018, 6, 16),
    efficiency: 84
  }));

  private get _chartContainer(): HTMLDivElement {
    return this._chartContainerRef.nativeElement;
  }

  private get _chartElement(): HTMLCanvasElement {
    return this._chartElementRef.nativeElement;
  }

  public ngAfterViewInit(): void {
    this.setupCanvas();
    this.setupChart();
  }

  private setupCanvas(): void {
    const computedStyles = window.getComputedStyle(this._chartContainer);
    const canvasWidth = [computedStyles.width, `-${computedStyles.paddingLeft}`, `-${computedStyles.paddingRight}`]
      .map((style: string): number => parseInt(style.replace('px', '')))
      .reduce((acc, curr) => acc += curr);
    this._chartElement.setAttribute('width', `${canvasWidth}px`);
    this._chartElement.setAttribute('height', '400px');
  }

  private setupChart(): void {
    const context: CanvasRenderingContext2D =
      this._chartElement.getContext('2d');
    const chart = new chartjs.Chart(context, {
      type: 'line',
      data: {
        labels: ['Ocak', 'Şubat', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım'],
        datasets: [{
          label: '# of Votes',
          data: [12, 19, 3, 5, 2, 3, 9],
          backgroundColor: '#BD200F',
          borderColor: '#BD200F',
          borderWidth: 1.5,
          fill: false,
          lineTension: 0,
          pointRadius: 0,
          pointHitRadius: 10
        }]
      },
      options: {
        legend: {
          display: false
        },
        tooltips: {
          mode: 'nearest'
        },
        scales: {
          yAxes: [{
            gridLines: {
              display: true,
              color: '#313237'
            },
            ticks: {
              beginAtZero: true
            }
          }],
          xAxes: [{
            gridLines: {
              display: false
            }
          }]
        }
      }
    });
  }

}
