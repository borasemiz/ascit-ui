import { Component, Input } from '@angular/core';

import { DevicePauseAnalysis, DeviceWorkTimeAnalysis } from 'src/app/shared/models';

@Component({
  selector: 'as-factory-average-analysis-table',
  templateUrl: 'factory-average-analysis-table.component.html',
  styleUrls: [ 'factory-average-analysis-table.component.scss' ]
})
export class FactoryAverageAnalysisTableComponent {

  public currentlyShown: {
    averageWaiting: number;
    factoryAverageWaiting: number;
    efficiency: number;
  } = null;

  @Input()
  private pauseAnalysis: DevicePauseAnalysis;

  @Input()
  private workTimeAnalysis: DeviceWorkTimeAnalysis;

  public switchTimeSpan(type: 'currentShift' | 'daily' | 'weekly' | 'monthly' | 'all'): void {
    if (this.pauseAnalysis) {
      switch(type) {
        case 'currentShift':
          this.currentlyShown = {
            averageWaiting: this.pauseAnalysis.currentShiftAmount,
            factoryAverageWaiting: this.pauseAnalysis.currentShiftTime,
            efficiency: this.fromNaNToZero(this.workTimeAnalysis.currentShiftTime / (this.workTimeAnalysis.currentShiftTime + this.pauseAnalysis.currentShiftTime)),
          };
          break;
        case 'daily':
          this.currentlyShown = {
            averageWaiting: this.pauseAnalysis.dailyAmount,
            factoryAverageWaiting: this.pauseAnalysis.dailyTime,
            efficiency: this.fromNaNToZero(this.workTimeAnalysis.dailyTime / (this.workTimeAnalysis.dailyTime + this.pauseAnalysis.dailyTime))
          };
          break;
        case 'weekly':
          this.currentlyShown = {
            averageWaiting: this.pauseAnalysis.weeklyAmount,
            factoryAverageWaiting: this.pauseAnalysis.weeklyTime,
            efficiency: this.fromNaNToZero(this.workTimeAnalysis.weeklyTime / (this.workTimeAnalysis.weeklyTime + this.pauseAnalysis.weeklyTime))
          };
          break;
        case 'monthly':
          this.currentlyShown = {
            averageWaiting: this.pauseAnalysis.monthlyAmount,
            factoryAverageWaiting: this.pauseAnalysis.monthlyTime,
            efficiency: this.fromNaNToZero(this.workTimeAnalysis.monthlyTime / (this.workTimeAnalysis.monthlyTime + this.pauseAnalysis.monthlyTime))
          };
          break;
        case 'all':
          this.currentlyShown = {
            averageWaiting: this.pauseAnalysis.allAmount,
            factoryAverageWaiting: this.pauseAnalysis.allTime,
            efficiency: this.fromNaNToZero(this.workTimeAnalysis.allTime / (this.workTimeAnalysis.allTime + this.pauseAnalysis.allTime))
          };
          break;
      }
    }
  }

  private fromNaNToZero(num: number): number {
    return Number.isNaN(num) ? 0 : num;
  }

}
