import { Component, OnInit, OnDestroy } from '@angular/core';

import { SocketService, DeviceStatisticsService } from './services';

@Component({
  selector: 'app-main',
  templateUrl: 'main.component.html',
  styleUrls: [ 'main.component.scss' ]
})
export class MainComponent implements OnInit, OnDestroy {

  constructor(
    private socketService: SocketService,
    private deviceStatisticsService: DeviceStatisticsService
  ) {}

  public async ngOnInit(): Promise<void> {
    window.document.body.classList.add('theme-default');
    this.deviceStatisticsService.initialize();
    this.socketService.initialize();
  }

  public ngOnDestroy(): void {
    window.document.body.classList.remove('theme-default');
  }

}
