import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { DeviceStatisticsService } from '../services';
import { DevicePauseAnalysis, ShortDeviceStatistic, DeviceWorkTimeAnalysis } from 'src/app/shared/models';

@Component({
  selector: 'app-machine-detail',
  templateUrl: 'machine-detail.component.html',
  styleUrls: [ 'machine-detail.component.scss' ]
})
export class MachineDetailComponent implements OnInit, OnDestroy {
  private _pauseStreamSubscription: Subscription;
  private _generalStatsStreamSubscription: Subscription;
  private _currentWorkTimeStatsStreamSubscription: Subscription;

  public currentPauseStats: DevicePauseAnalysis;
  public generalStatus: ShortDeviceStatistic;
  public currentWorkTimeStats: DeviceWorkTimeAnalysis;

  public get currentShiftEfficiency(): number {
    const efficiency = 100 * this.currentWorkTimeStats.currentShiftTime / (this.currentWorkTimeStats.currentShiftTime + this.currentPauseStats.currentShiftTime);
    if (Number.isNaN(efficiency)) {
      return 0;
    }
    return efficiency;
  }

  constructor(
    private deviceStatisticsService: DeviceStatisticsService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  public ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this._pauseStreamSubscription = this.deviceStatisticsService.pauseStatsStream$.subscribe(stat => {
        this.currentPauseStats = stat[params['machineId']];
      });
      this._generalStatsStreamSubscription = this.deviceStatisticsService.generalStatsStream$.subscribe(stats => {
        this.generalStatus = stats[params['machineId']];
      });
      this._currentWorkTimeStatsStreamSubscription = this.deviceStatisticsService.workTimeStatsStream$.subscribe(stats => {
        this.currentWorkTimeStats = stats[params['machineId']];
      });
    });
  }

  public ngOnDestroy(): void {
    if (this._pauseStreamSubscription) {
      this._pauseStreamSubscription.unsubscribe();
    }
  }
}
