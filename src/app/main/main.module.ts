import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { MainRoutingModule } from './main-routing.module';
import { MachineListComponent } from './machine-list/machine-list.component';
import { MainComponent } from './main.component';
import {
  NavbarComponent,
  MachineCardComponent,
  BreadcrumbsComponent,
  MachineWaitingTableComponent,
  FactoryAverageAnalysisTableComponent,
  ProductionHistoryComponent,
} from './components';
import { SharedModule } from '../shared/shared.module';
import { MachineDetailComponent } from './machine-detail/machine-detail.component';
import { SocketService, DeviceStatisticsService, ProductService } from './services';

@NgModule({
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
    HttpClientModule
  ],
  declarations: [
    MainComponent,
    MachineListComponent,
    MachineDetailComponent,
    NavbarComponent,
    MachineCardComponent,
    BreadcrumbsComponent,
    MachineWaitingTableComponent,
    FactoryAverageAnalysisTableComponent,
    ProductionHistoryComponent
  ],
  providers: [
    SocketService,
    DeviceStatisticsService,
    ProductService
  ]
})
export class MainModule {}
