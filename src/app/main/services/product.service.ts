import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { ProductInfo } from 'src/app/shared/models';
import { environment } from 'src/environments/environment';
import { CanActivate } from '@angular/router';

@Injectable()
export class ProductService implements CanActivate {
  private _products: { [productId: string]: ProductInfo };

  constructor(
    private http: HttpClient
  ) {}

  public async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    try {
      await this.initProducts();
      return true;
    } catch(e) {
      console.error(e);
      return true;
    }
  }

  public async initProducts(): Promise<void> {
    const products: ProductInfo[] = await this.http.get<any>(`${environment.serverUrl}/products`).toPromise();
    this._products = products.reduce((acc: { [productId: string]: ProductInfo }, cur: ProductInfo) => {
      acc[cur.productId + ''] = cur;
      return acc
    }, {});
  }

  public getProduct(productId: number): ProductInfo {
    return this._products[productId + ''];
  }

}
