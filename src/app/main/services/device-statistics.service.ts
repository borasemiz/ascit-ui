import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription, Observable } from 'rxjs';
import { addDays } from 'date-fns';

import { LogService } from './log.service';
import { DeviceMessageLog, ShortDeviceStatistic, DevicePauseAnalysis, DeviceWorkTimeAnalysis } from '../../shared/models';
import { ProductService } from './product.service';

@Injectable()
export class DeviceStatisticsService {
  private _generalStats: { [machineId: string]: ShortDeviceStatistic } = {};
  private _pauseStats: { [machineId: string]: DevicePauseAnalysis } = {};
  private _workTimeStats: { [machineId: string]: DeviceWorkTimeAnalysis } = {}; 

  private _generalStatsStream$ = new BehaviorSubject<{
    [machineId: string]: ShortDeviceStatistic
  }>({});
  private _pauseStatsStream$ = new BehaviorSubject<{
    [machineId: string]: DevicePauseAnalysis
  }>({});
  private _workTimeStatsStream$ = new BehaviorSubject<{
    [machineId: string]: DeviceWorkTimeAnalysis
  }>({});

  private _logSubscription: Subscription;

  constructor(
    private logService: LogService,
    private productService: ProductService
  ) {}

  public get generalStatsStream$(): Observable<{ [machineId: string]: ShortDeviceStatistic }> {
    return this._generalStatsStream$.asObservable();
  }

  public get pauseStatsStream$(): Observable<{ [machineId: string]: DevicePauseAnalysis }> {
    return this._pauseStatsStream$.asObservable();
  }

  public get workTimeStatsStream$(): Observable<{ [machineId: string]: DeviceWorkTimeAnalysis }> {
    return this._workTimeStatsStream$.asObservable();
  }

  public initialize(): void {
    this._logSubscription = this.logService.getStream().subscribe({
      next: log => this.onLogReceived(log),
      error: error => console.error(error)
    });
  }

  private onLogReceived(log: DeviceMessageLog): void {
    this.extractGeneralStats(log);
    this.extractDevicePauseStats(log);
    this.extractDeviceWorkTimeStats(log);
  }

  private extractGeneralStats(log: DeviceMessageLog): void {
    this._generalStats[log.machineId + ''] = {
      machineId: log.machineId,
      rpm: log.rpm,
      lastReceivedInfoDate: log.receivedOn,
      currentProductionAmount: this.calculateCurrentProductionAmount(log),
      productName: this.productService.getProduct(log.productId).description,
      targetProductionAmount: this.calculateTargetProductionAmount(log),
      isWorking: log.isEnabled
    }
    this._generalStatsStream$.next(this._generalStats);
  }

  private extractDevicePauseStats(log: DeviceMessageLog): void {
    const now = new Date();
    if (!this._pauseStats[log.machineId + '']) {
      this._pauseStats[log.machineId + ''] = new DevicePauseAnalysis();
    }
    this.calculatePauseStatForLog(this._pauseStats[log.machineId + ''], log);
    this._pauseStatsStream$.next(this._pauseStats);
  }

  private extractDeviceWorkTimeStats(log: DeviceMessageLog): void {
    const now = new Date();
    if (!this._workTimeStats[log.machineId + '']) {
      this._workTimeStats[log.machineId + ''] = new DeviceWorkTimeAnalysis();
    }
    this.calculateWorkTimeStatsForLog(this._workTimeStats[log.machineId + ''], log);
    this._workTimeStatsStream$.next(this._workTimeStats);
  }

  private calculateWorkTimeStatsForLog(stat: DeviceWorkTimeAnalysis, current: DeviceMessageLog): void {
    const now = Date.now();
    if (!stat.previousLog) {
      stat.previousLog = current;
    }
    if (current.isEnabled) {
      stat.allTime += current.receivedOn - stat.previousLog.receivedOn;
      stat.monthlyTime += (current.receivedOn >= addDays(now, -30).valueOf()) ? current.receivedOn - stat.previousLog.receivedOn : 0;
      stat.weeklyTime += (current.receivedOn >= addDays(now, -7).valueOf()) ? current.receivedOn - stat.previousLog.receivedOn : 0;
      stat.dailyTime += (current.receivedOn >= addDays(now, -1).valueOf()) ? current.receivedOn - stat.previousLog.receivedOn : 0;
      stat.currentShiftTime = stat.dailyTime;
    }
    stat.previousLog = current;
  }

  private calculatePauseStatForLog(stat: DevicePauseAnalysis, current: DeviceMessageLog): void {
    const now = Date.now();
    if (!stat.previousLog) {
      stat.previousLog = current;
    }
    if (!current.isEnabled) {
      stat.allTime += current.receivedOn - stat.previousLog.receivedOn;
      stat.monthlyTime += (current.receivedOn >= addDays(now, -30).valueOf()) ? current.receivedOn - stat.previousLog.receivedOn : 0;
      stat.weeklyTime += (current.receivedOn >= addDays(now, -7).valueOf()) ? current.receivedOn - stat.previousLog.receivedOn : 0;
      stat.dailyTime += (current.receivedOn >= addDays(now, -1).valueOf()) ? current.receivedOn - stat.previousLog.receivedOn : 0;
      stat.currentShiftTime = stat.dailyTime;
    }
    if (!stat.previousLog.isEnabled && current.isEnabled) {
      stat.allAmount += 1;
      stat.monthlyAmount += (current.receivedOn >= addDays(now, -30).valueOf()) ? 1 : 0;
      stat.weeklyAmount += (current.receivedOn >= addDays(now, -7).valueOf()) ? 1 : 0;
      stat.dailyAmount += (current.receivedOn >= addDays(now, -1).valueOf()) ? 1 : 0;
      stat.currentShiftAmount = stat.dailyAmount;
    }
    stat.previousLog = current;
  }

  private calculateCurrentProductionAmount(log: DeviceMessageLog): number {
    const prodInfo = this.productService.getProduct(log.productId);
    const part1 = log.currentStats.horizontalBakla * log.currentStats.verticalBakla * 25 / 10000;
    return part1 * prodInfo.weight * log.currentStats.ballCount;
  }

  private calculateTargetProductionAmount(log: DeviceMessageLog): number {
    const prodInfo = this.productService.getProduct(log.productId);
    const part1 = log.targetStats.verticalBakla * log.targetStats.horizontalBakla * 25 / 10000;
    return part1 * prodInfo.weight * log.targetStats.ballCount;
  }

}
