import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { AuthService } from '../../shared/services';
import { DeviceMessageLog } from '../../shared/models';

@Injectable()
export class LogService {
  private logStream$: Subject<DeviceMessageLog> = new Subject();
  private socket: WebSocket;

  constructor(
    private authService: AuthService,
    private router: Router,
    private httpClient: HttpClient
  ) { }

  public getStream(): Observable<DeviceMessageLog> {
    return this.logStream$.asObservable();
  }

  public async initialize(): Promise<void> {
    await this.fetchAllLogs();
    await this.initSocket();
  }

  private async fetchAllLogs(): Promise<void> {
    const logs = await this.httpClient.get<DeviceMessageLog[]>(`${environment.serverUrl}/logs`).toPromise();
    logs.forEach(log => this.logStream$.next(log));
  }

  private async initSocket(): Promise<void> {
    try {
      await this.openSocketConnection();
      this.socket.addEventListener('close', () => console.log('WS Connection Closed!'));
      this.socket.addEventListener('error', (err) => console.log('WS Connection error: ', err));
      this.socket.addEventListener('message', (event) => this.onSocketMessageReceived(event.data));
    } catch (e) {
      this.closeSocketClient();
      this.router.navigateByUrl('/auth/login');
    }
  }

  private onSocketMessageReceived(message: string): void {
    try {
      const log: DeviceMessageLog = JSON.parse(message);
      this.logStream$.next(log);
      console.log(log);
    } catch(e) {
      // Expected
    }
  }

  private authenticateSocket(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.socket.onmessage = event => {
        this.socket.onmessage = null;
        const authResult = event.data as string;
        if (authResult !== 'OK') {
          reject(new Error('Authentication failed!'));
        } else {
          resolve();
        }
      };
      this.socket.send(JSON.stringify({
        token: this.authService.credentials.token
      }));
    });
  }

  private openSocketConnection(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.socket = new WebSocket(environment.websocketUrl);
      this.socket.addEventListener('open', async () => {
        try {
          await this.authenticateSocket();
          console.log('WS Connection Established!');
          resolve();
        } catch (e) {
          reject(e);
        }
      });
    });
  }

  private closeSocketClient(): void {
    this.socket.onclose = () => {};
    this.socket.close();
  }
}
